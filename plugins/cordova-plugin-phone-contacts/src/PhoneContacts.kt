package cordova.plugin

import android.Manifest
import android.util.Log
import org.apache.cordova.CallbackContext
import org.apache.cordova.CordovaPlugin
import org.apache.cordova.PermissionHelper
import org.json.JSONArray
import org.json.JSONException

class PhoneContacts : CordovaPlugin() {
    lateinit var context: CallbackContext
    val READ = Manifest.permission.READ_CONTACTS

    @Throws(JSONException::class)
    override fun execute(action: String, data: JSONArray, callbackContext: CallbackContext): Boolean {
        context = callbackContext
        var result = true
        try {
            if (action == "dataContacts") {

                val contactAccessor = ContactAccessor(this.cordova)
                if (PermissionHelper.hasPermission(this, READ)) {
                    val contactslist = contactAccessor.getContacts()
                    val contacts = JSONArray(contactslist)
                    callbackContext.success(contacts)
                } else {
                    PermissionHelper.requestPermission(this, 0, READ)
                }

            } else {
                handleError("Invalid action")
                result = false
            }
        } catch (e: Exception) {
            handleException(e)
            result = false
        }

        return result
    }


    /**
     * Handles an error while executing a plugin API method.
     * Calls the registered Javascript plugin error handler callback.
     *
     * @param errorMsg Error message to pass to the JS error handler
     */
    private fun handleError(errorMsg: String) {
        try {
            Log.e(TAG, errorMsg)
            context.error(errorMsg)
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }

    }

    private fun handleException(exception: Exception) {
        handleError(exception.toString())
    }

    companion object {

        protected val TAG = "PhoneContacts"
    }
}
