cordova.define("cordova-plugin-phone-contacts.phonecontacts", function(require, exports, module) {
/*global cordova, module*/

module.exports = {
    getContacts: function (input, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "PhoneContacts", "dataContacts", [input]);
    }
};

});
