/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready

/*** Funcionalidad de lector de QR ***/
function scanQR() {
    var params = {
        'prompt_message': 'Scan a barcode', // Change the info message. A blank message ('') will show a default message
        'orientation_locked': true, // Lock the orientation screen
        'camera_id': 0, // Choose the camera source
        'beep_enabled': true, // Enables a beep after the scan
        'scan_type': 'normal', // Types of scan mode: normal = default black with white background / inverted = white bars on dark background / mixed = normal and inverted modes
        'barcode_formats': [
            'QR_CODE',
            'CODE_39',
            'CODE_128'], // Put a list of formats that the scanner will find. A blank list ([]) will enable scan of all barcode types
        'extras': {} // Additional extra parameters. See [ZXing Journey Apps][1] IntentIntegrator and Intents for more details
    }
    cordova.plugins.scanqrkt.scan(params, function (response) {
        console.log(response)
        $('#labelResult').text('Resultado: ' + response)
        $('#list-cards').show('swing');
    }, function (error) {
        $('#labelResult').text('Error: ' + error)
    })
}

function requestPermission(permissions, callback, flag) {

    cordova.plugins.permissions.checkPermission(permissions, function (response) {
        if (response.hasPermission) {
            callback();
        } else {
            if (flag) {
                cordova.plugins.permissions.requestPermissions(permissions, success, error);
            } else {
                cordova.plugins.permissions.requestPermission(permissions, success, error);
            }
        }
    });

    function success(response) {
        if (response.hasPermission) {
            callback();
        } else {
            error(response)
        }
    }

    function error(error) {
        $('#loading').hide();
        alert(error)
    }


}

/*** Funcionalidad de lista de contactos ***/
function getContacts() {
    cordova.plugins.contacts.getContacts('', success, error)

    function success(response) {
        $('#loading').hide();
        var $list = $('<ul class="list-group"></ul>')
        for (var i = 0; i < response.length; i++) {
            var contact= JSON.parse(response[i])
            $('<li class="list-group-item d-flex justify-content-between align-items-start">' +
                '<div class="ms-2 me-auto">' +
                '<div class="fw-bold">' + contact.name + '</div>' +
                contact.mobileNumber +
                '</div>' +
                '<i class="fa fa-phone fa-2x link-success"></i>' +
                '</li>').appendTo($list)
            $list.appendTo('#areaContacts')
        }
    }

    function error(error) {
        $('#loading').hide();
        alert(error)
    }
}

/*** Funcionalidad de localización ***/
function initMap() {
    let map;
    if (navigator.geolocation) { //check if geolocation is available
        navigator.geolocation.getCurrentPosition(success, error);
    } else {
        alert('No fue posible obtener la ubicación')
    }

    function success(position) {
        map = new google.maps.Map(document.getElementById("map"), {
            center: {lat: position.coords.latitude, lng: position.coords.longitude},
            zoom: 15,
        });

        new google.maps.Marker({
            map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: {lat: position.coords.latitude, lng: position.coords.longitude},
        });

        google.maps.event.addListenerOnce(map, 'idle', () => {
            $('#loading').hide();
        });
    }

    function error() {
        $('#loading').hide();
        alert('No fue posible obtener la ubicación')
    }
}

$(document).ready(function () {
    $("#btnScan").click(function () {
        scanQR();
    });

    $("#btnContact").click(function () {
        $('#loading').show()
        $('#areaContacts').empty();
        requestPermission(cordova.plugins.permissions.READ_CONTACTS, getContacts, false);
    });

    $("#btnGeo").click(function () {
        $('#loading').show()
        var list = [
            cordova.plugins.permissions.ACCESS_COARSE_LOCATION,
            cordova.plugins.permissions.ACCESS_FINE_LOCATION
        ];
        requestPermission(list, initMap, true);
    });

    $(".nav-item").click(function (ref) {
        $('.block').hide();
        if (ref.currentTarget.dataset.link === 'btnLocation') {
            $('.' + ref.currentTarget.dataset.link).toggle()
        } else {
            $('#' + ref.currentTarget.dataset.link).toggle()
        }
    });

});


