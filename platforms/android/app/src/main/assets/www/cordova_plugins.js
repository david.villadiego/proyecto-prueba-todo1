cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "cordova-plugin-android-permissions.Permissions",
      "file": "plugins/cordova-plugin-android-permissions/www/permissions.js",
      "pluginId": "cordova-plugin-android-permissions",
      "clobbers": [
        "cordova.plugins.permissions"
      ]
    },
    {
      "id": "cordova-plugin-phone-contacts.phonecontacts",
      "file": "plugins/cordova-plugin-phone-contacts/www/phonecontacts.js",
      "pluginId": "cordova-plugin-phone-contacts",
      "clobbers": [
        "cordova.plugins.contacts"
      ]
    },
    {
      "id": "cordova-plugin-scanqrkt.scanqrkt",
      "file": "plugins/cordova-plugin-scanqrkt/www/scanqrkt.js",
      "pluginId": "cordova-plugin-scanqrkt",
      "clobbers": [
        "cordova.plugins.scanqrkt"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-android-permissions": "1.1.2",
    "cordova-plugin-phone-contacts": "1.0.0",
    "cordova-plugin-scanqrkt": "1.0.0",
    "cordova-plugin-whitelist": "1.3.4"
  };
});